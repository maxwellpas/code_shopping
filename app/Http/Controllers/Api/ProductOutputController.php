<?php

namespace CodeShopping\Http\Controllers\Api;

use CodeShopping\Http\Controllers\Controller;
use CodeShopping\Http\Requests\ProductOutputRequest;
use CodeShopping\Http\Resources\ProductOutputResource;
use CodeShopping\Models\ProductOutput;
use Illuminate\Http\Request;

class ProductOutputController extends Controller
{

    public function index()
    {
        // with - "eager loading, por padrão o laravel usa o lazy loading" Melhora a performace, carregamento retardado, acessa menos o BD
        // Para usar mais de um relacionamento, é só usar 'product','product.categories' - realcionamento de um relacionamento
        $output = ProductOutput::with('product')->paginate();
        return ProductOutputResource::collection($output);
    }


    public function store(ProductOutputRequest $request)
    {
        $input = ProductOutput::create($request->all());
        return new ProductOutputResource($input);
    }


    public function show(ProductOutput $output)
    {
        return new ProductOutputResource($output);
    }

}
