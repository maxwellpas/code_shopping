<?php

namespace CodeShopping\Http\Controllers\Api;

use CodeShopping\Http\Controllers\Controller;
use CodeShopping\Http\Requests\ProductInputRequest;
use CodeShopping\Http\Resources\ProductInputResource;
use CodeShopping\Models\ProductInput;
use Illuminate\Http\Request;

class ProductInputController extends Controller
{

    public function index()
    {
        // with - "eager loading, por padrão o laravel usa o lazy loading" Melhora a performace, carregamento retardado, acessa menos o BD
        // Para usar mais de um relacionamento, é só usar 'product','product.categories' - realcionamento de um relacionamento
        $inputs = ProductInput::with('product')->paginate();
        return ProductInputResource::collection($inputs);
    }


    public function store(ProductInputRequest $request)
    {
        $input = ProductInput::create($request->all());
        return new ProductInputResource($input);
    }


    public function show(ProductInput $input)
    {
        return new ProductInputResource($input);
    }

}
