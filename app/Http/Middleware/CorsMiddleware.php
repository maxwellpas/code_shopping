<?php

namespace CodeShopping\Http\Middleware;

use Closure;

class CorsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // barryvdh/laravel-cors - biblioteca de terceiro para ajudar, não foi implementada
        //if( $request->is('api/*') && ($request->method() == 'OPTIONS') ){
        if( $request->is('api/*') ){
            header('Access-Control-Allow-Origin: *'); // Se colocar um endereço específico, vai ser aceito somente desse endereço informado
            header('Access-Control-Allow-Headers: Content-Type, Authorization');
        }

        return $next($request);
    }
}
