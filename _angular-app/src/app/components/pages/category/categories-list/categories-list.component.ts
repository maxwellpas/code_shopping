import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.css']
})
export class CategoriesListComponent implements OnInit {

  categories: Array<{id: number, name: string, active: boolean, created_at: {date: string}}> = [];

  constructor(private http: HttpClient) { }

  ngOnInit() {
      const token = window.localStorage.getItem('token');
      this.http.get<{data: Array<{id: number, name: string, active: boolean, created_at: {date: string}}>}>('http://localhost:8000/api/categories',{
          headers: {
              'Authorization': `Bearer ${token}`
          }
      })
          .subscribe(response => {
              response.data[0].active = false; // forçando um false, excluir depois
              this.categories = response.data
          });
  }

}
