import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Router } from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    //
    // email = "admin@user.com";
    //
    // obj = {
    //   email: 'outroemail.com',
    //   array: [1]
    // }

    credentials = {
        email: 'admin@user.com',
        password: 'secret'

    }

    showMessageError = false;

    // simbolo [] - O TS refrele teltteraçãos no template   Dados ---> Template


    constructor(private http: HttpClient, private router: Router) {

    }

    ngOnInit() {
        // setTimeout(() =>{
        //   this.email = "Qualquer coisa";
        // }, 3000)
    }


    submit(){
        this.http.post<any>('http://localhost:8000/api/login', this.credentials)
            .subscribe( (data) => {
                const token = data.token;
                window.localStorage.setItem('token', token);
                this.router.navigate(['categories/list']);

            }, () => this.showMessageError = true );

        return false;
    }

    // meclicou($event){
    //   console.log($event);
    // }
    //
    // digitou($event){
    //   console.log($event);
    // }

}
